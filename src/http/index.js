import axios from "axios"
import { Message } from 'element-ui';
const instance = axios.create({
    baseURL: "http://localhost:8888/api/private/v1/"
})
export function http(url, method, data, params) {
    return new Promise((resolve, reject) => {
        instance({
            url,
            method,
            data,
            params
        }).then(res => {
            if ((res.status >= 200 && res.status < 300) || res.status === 304) {
                if (res.data.meta.status === 200) {
                    Message({
                        showClose: true,
                        message: "欢迎登陆"+res.data.data.username,
                        type: 'success'
                      });
                    resolve(res.data.data)
                } else {
                    Message({
                        showClose: true,
                        message:res.data.meta.msg,
                        type: 'error'
                      });
                    reject(res)
                }
            } else {
                reject(res)
            }
        }).catch(err => {
            reject(err)
        })
    })
}