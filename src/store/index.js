import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:localStorage.getItem("token") || "",
    username:localStorage.getItem("username") || ""
  },
  mutations: {
    setToken(state,data){
      state.token=data;
      localStorage.setItem("token",data)
    },
    setUsername(state,data){
      state.tokenusername=data;
      localStorage.setItem("username",data)
    }
  },
  actions: {
  },
  modules: {
  }
})
